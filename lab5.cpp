#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {

  //seeding random generator
  srand(unsigned (time(0)));

	//setting up the cards
	Card card[52];
	int i = 0;
	
	//card initialization
	for (int j = 0; j < 4; j++){
		for (int k = 1; k < 14; k++){
			card[i].suit = static_cast<Suit>(j);
			card[i].value = k;
			++i;
		}
	}


	//shuffling the deck
	random_shuffle(&card[0], &card[52], myrandom);

	//givcing you a hand
	Card hand[5] = {card[0], card[1], card[2], card[3], card[4]};

  //sorting the hand
	sort(&hand[0], &hand[5], suit_order);

  //printing out your hand
	for (int i = 0; i < 5; ++i){
		cout << setw(10) << get_card_name(hand[i]) 
		<< " of " << get_suit_code(hand[i]) << endl;
	}

  return 0;
}


//sorting algorithm for your hand
bool suit_order(const Card& lhs, const Card& rhs) {
  if (lhs.suit < rhs.suit){
  	return true;
  }
  else if ((lhs.suit == rhs.suit) && (lhs.value < rhs.value)){
  	return true;
  }
  else {return false;}
}

//turns suit name into symbol
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//returns what to print based on card value
string get_card_name(Card& c) {
  switch(c.value) {
  	case 1:		return "Ace";
	case 2:		return "2";
	case 3: 	return "3";
	case 4: 	return "4";
	case 5: 	return "5";
	case 6: 	return "6";
	case 7: 	return "7";
	case 8: 	return "8";
	case 9: 	return "9";
	case 10: 	return "10";
  	case 11:	return "Jack";
  	case 12:	return "Queen";
  	case 13:	return "King";
  	default:	return " ";
  }
}











